import React, { useState, useEffect, useReducer, useContext, useRef } from 'react';

import Card from '../UI/Card/Card';
import Input from '../UI/Input/Input'
import classes from './Login.module.css';
import Button from '../UI/Button/Button';
import AuthContext from '../../store/auth-context'

const emailReducer = (state, action) => {
  if (action.type === 'USER_INPUT') {
    return {
      value: action.inputValue,
      isValid: action.inputValue.includes('@')
    }
  }

  if (action.type === 'INPUT_BLUR') {
    return {
      value: state.value,
      isValid: state.value.includes('@')
    }
  }

  return {
    value: '',
    isValid: false
  }
}

const passwordReducer = (state, action) => {
  if (action.type === 'USER_INPUT') {
    return {
      value: action.inputValue,
      isValid: action.inputValue.trim().length > 6
    }
  }

  if (action.type === 'INPUT_BLUR') {
    return {
      value: state.value,
      isValid: state.value.trim().length > 6
    }
  }

  return {
    value: '',
    isValid: false
  }
}

const Login = (props) => {
  // const [enteredEmail, setEnteredEmail] = useState('');
  // const [emailIsValid, setEmailIsValid] = useState();
  // const [enteredPassword, setEnteredPassword] = useState('');
  // const [passwordIsValid, setPasswordIsValid] = useState();
  const context = useContext(AuthContext)

  const emailRef = useRef()
  const passwordRef = useRef()

  const [formIsValid, setFormIsValid] = useState(false);

  const [emailState, emailDispatch] = useReducer(
    emailReducer,
    {
      value: '',
      isValid: null
    }
  )

  const [passwordState, passwordDispatch] = useReducer(
    passwordReducer,
    {
      value: '',
      isValid: null
    }
  )

  const { isValid: emailIsValid } = emailState
  const { isValid: pwdIsValid } = passwordState

  useEffect(() => {
    const timerIdentifier = setTimeout(() => {
      console.log('VALIDATING')
      setFormIsValid(
        emailIsValid && pwdIsValid
        // emailState.isValid && passwordState.isValid
      )
    }, 500)

    return () => {
      console.log('timer cleanup')
      clearTimeout(timerIdentifier)
    }
  }, [emailIsValid, pwdIsValid])
  // }, [emailState.isValid, passwordState.isValid])


  useEffect(() => {
    console.log('sample effect running')
    return () => {
      console.log('returned effect')
    }
  }, [])

  // >>> notes: useEffect FUNCTION BEHAVIOR
  // >>> useEffect() will always run ONCE when the component was first rendered.
  // >>> And if useEffect() returns a function, that returned function will be held together with useEffect()'s dependencies and the returned function will be ran when the values of the dependencies have changed.

  // >>> notes: useEffect DEPENDENCY BEHAVIOR
  // >>> useEffect() takes an array of state variables as dependencies (think "watchers" in VueJS) as second argument.
  // >>> So when either of the values is changed, the first argument (the function) will be run.
  // >>> If an EMPTY array is being passed in, useEffect() will only be run ONCE -- that is when the component FIRST renders.
  // >>> If NOTHING is passed in at all (not even an array, no second argument), the useEffect() will always be run EVERYTIME the component re-renders.

  const emailChangeHandler = (event) => {
    // setEnteredEmail(event.target.value);
    emailDispatch({
      type: 'USER_INPUT',
      inputValue: event.target.value
    })

    // setFormIsValid(
    //   event.target.value.includes('@') && passwordState.isValid
    // )
  };

  const passwordChangeHandler = (event) => {
    // setEnteredPassword(event.target.value);
    passwordDispatch({
      type: 'USER_INPUT',
      inputValue: event.target.value
    })

    // setFormIsValid(
    //   emailState.isValid && event.target.value.trim().length > 6
    // )
  };

  const validateEmailHandler = () => {
    // setEmailIsValid(emailState.isValid);
    emailDispatch({
      type: 'INPUT_BLUR'
    })
  };

  const validatePasswordHandler = () => {
    // setPasswordIsValid(enteredPassword.trim().length > 6);
    passwordDispatch({
      type: 'INPUT_BLUR'
    })
  };

  const submitHandler = (event) => {
    event.preventDefault();

    if (formIsValid) {
      context.onLogin(emailState.value, passwordState.value);
    } else if (!emailIsValid) { // well can't use false here because initial state is null
      emailRef.current.focusOnErrorField()
    } else {
      passwordRef.current.focusOnErrorField()
    }
  };

  return (
    <Card className={classes.login}>
      <form onSubmit={submitHandler}>
        <Input
          ref={emailRef}
          label="E-mail"
          id="email"
          type="email"
          checkState={emailState}
          onChange={emailChangeHandler}
          onBlur={validateEmailHandler}
        />
        <Input
          ref={passwordRef}
          label="Password"
          id="password"
          type="password"
          checkState={passwordState}
          onChange={passwordChangeHandler}
          onBlur={validatePasswordHandler}
        />
        <div className={classes.actions}>
          <Button type="submit" className={classes.btn}>
            Login
          </Button>
        </div>
      </form>
    </Card>
  );
};

export default Login;
