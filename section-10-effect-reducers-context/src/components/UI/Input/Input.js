import { useRef, useImperativeHandle, forwardRef } from 'react'
import classes from './Input.module.css'

const Input = forwardRef((props, ref) => {
  const inputRef = useRef()

  const activate = () => {
    inputRef.current.focus()
  }

  useImperativeHandle(ref, () => {
    return {
      focusOnErrorField: activate
    }
  })

  // >>> notes:
  // >>> The callback function in useImperativeHandle() is a function that returns an object that "creates the connection" which the key can be accessed from the OUTSIDE of this component, and the value will be pointed to something INSIDE of the component.

  return (
    <div
      className={`${classes.control} ${
        props.checkState.isValid === false ? classes.invalid : ''
      }`}
    >
      <label htmlFor={props.id}>{props.label}</label>
      <input
        ref={inputRef}
        id={props.id}
        type={props.type}
        value={props.checkState.value}
        onChange={props.onChange}
        onBlur={props.onBlur}
      />
    </div>
  )
})

export default Input
