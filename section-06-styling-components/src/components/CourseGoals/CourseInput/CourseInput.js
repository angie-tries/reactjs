import React, { useState } from 'react';

import Button from '../../UI/Button/Button';
import styles from './CourseInput.module.css';

// import StyledComponent from 'styled-components'

// const FormControl = StyledComponent.div`
//   margin: 0.5rem 0;

//   & label {
//     font-weight: bold;
//     display: block;
//     margin-bottom: 0.5rem;
//     color: ${props => props.inputEmpty ? 'red' : 'black'}
//   }

//   & input {
//     display: block;
//     width: 100%;
//     border: 1px solid ${props => props.inputEmpty ? 'red' : '#ccc'};
//     background-color: ${props => props.inputEmpty ? '#ff7d7d' : 'transparent'};
//     font: inherit;
//     line-height: 1.5rem;
//     padding: 0 0.25rem;
//   }

//   & input:focus {
//     outline: none;
//     background: #fad0ec;
//     border-color: #8b005d;
//   }
// `

const CourseInput = props => {
  const [enteredValue, setEnteredValue] = useState('');
  const [isEmptyInput, setIsEmptyInput] = useState(false)

  const goalInputChangeHandler = event => {
    if (event.target.value.trim() !== '') {
      setIsEmptyInput(false)
    }
    setEnteredValue(event.target.value);
  };

  const formSubmitHandler = event => {
    event.preventDefault();
    if (enteredValue.trim() === '') {
      setIsEmptyInput(true)
      return
    }
    props.onAddGoal(enteredValue);
  };

  return (
    <form onSubmit={formSubmitHandler}>
      <div
        className={`${styles['form-control']} ${isEmptyInput ? styles.invalid : ''}`}>
        <label>Course Goal</label>
        <input
          type="text"
          onChange={goalInputChangeHandler} />
      </div>
      <Button type="submit">Add Goal</Button>
    </form>
  );
};

export default CourseInput;

// notes:
// >>> Styled components turns every css class selector into a method that is tied to the main module imported object that can be referred to.
// >>> This ensures uniqueness for every class. (Inspect in browser to know more.)
