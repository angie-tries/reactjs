import './Chart.css'
import Chartbar from './Chartbar'

const Chart = props => {
  const valuesFromDataPoint = props.dataPoints.map(el => el.value)
  const maxFromSum = Math.max(...valuesFromDataPoint)

  return (
    <div className="chart">
      {props.dataPoints.map((el) => {
        return (
          <Chartbar
            key={el.label}
            value={el.value}
            maxValue={maxFromSum}
            label={el.label}
          />
        )
      })}
    </div>
  )
}

export default Chart
