import './NewExpense.css'
import { useState } from 'react'
import ExpenseForm from './ExpenseForm'

const NewExpense = props => {
  const [showForm, setShowForm] = useState(false)

  const showInput = () => {
    setShowForm(true)
  }

  const hideInput = event => {
    event.preventDefault()
    setShowForm(false)
  }

  const dataSubmissionHandler = dataFromExpenseForm => {
    const newExpenseObj = {
      ...dataFromExpenseForm,
      id: Math.random().toFixed(2)
    }

    props.insertNewData(newExpenseObj)
    hideInput()
  }


  return (
    <div className="new-expense">
      {showForm ? (
        // Even in ternary, you can only return ONE element.
        <ExpenseForm
          submitNewEntries={dataSubmissionHandler}
          hideInput={hideInput}
        />
      ) : (
        <button onClick={showInput}>Add new expense</button>
      )}
    </div>
  )
}

export default NewExpense
