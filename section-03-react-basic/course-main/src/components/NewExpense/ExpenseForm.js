import './ExpenseForm.css'
import dayjs from 'dayjs'
import React, { useState } from 'react'

const ExpenseForm = props => {
  const [title, setTitle]   = useState('')
  const [amount, setAmount] = useState('0.01')
  const [date, setDate]     = useState('2019-01-01')
  // const [userInput, setUserInput] = useState({
  //   title: '',
  //   amount: '',
  //   date: ''
  // })

  const titleChangeHandler = event => {
    setTitle(event.target.value)

    // setUserInput({
    //   ...userInput,
    //   title: event.target.value
    // })

    // >>> notes:
    // >>> Multiple state variables are just one big object containing all the variables as kv pairs.
    // >>> Updating the kv pair can be easily done with ES6 object destructuring using spread operator.
    // >>> Just make sure the intended value to update is being refereced last, so it will replace the existing value.
    // >>> "Callback style"

    // setUserInput(prevState => {
    //   return {
    //     ...prevState,
    //     title: event.target.value
    //   }
    // })

    // >>> notes:
    // >>> There are use cases where one update depends on another.
    // >>> And because React "schedules" the update function, to make sure they are in the correct order,
    // >>> instead of passing in the updated state into the update function, pass in an anon function to, return the updated state instead!
    // >>> This is called "function form"
  }

  const amountChangeHandler = event => {
    setAmount(event.target.value)
    // setUserInput({
    //   ...userInput,
    //   amount: event.target.value
    // })
  }

  const dateChangeHandler = event => {
    setDate(event.target.value)
    // setUserInput({
    //   ...userInput,
    //   date: event.target.value
    // })
  }

  const submitHandler = event => {
    event.preventDefault()

    const ExpenseData = {
      title,
      amount: +amount, // + converts string number to an actual number! so it's equavalent as Number(amount)
      date: dayjs(date)
    }

    props.submitNewEntries(ExpenseData)

    setTitle('')
    setAmount('')
    setDate('')
    props.hideInput()
  }


  return (
    <form onSubmit={submitHandler}>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Title</label>
          <input
            type="text"
            onChange={titleChangeHandler}
            value={title}
          />
        </div>
        <div className="new-expense__control">
          <label>Amount</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            onChange={amountChangeHandler}
            value={amount}
          />
        </div>
        <div className="new-expense__control">
          <label>Date</label>
          <input
            type="date"
            min="2019-01-01"
            max="2022-12-31"
            onChange={dateChangeHandler}
            value={date}
          />
        </div>
      </div>
      <div className="new-expense__actions">
        <button onClick={props.hideInput}>Cancel</button>
        <button type="submit">Add Expense</button>
        {/* <input type="button" value="Click me to cancel" onClick={props.hideInput}/> */}
      </div>
    </form>
  )
}

export default ExpenseForm
