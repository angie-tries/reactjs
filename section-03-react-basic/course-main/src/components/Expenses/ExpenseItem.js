import './ExpenseItem.css'
// >>> notes:
// >>> useXxxx are all react hooks.
// >>> React hooks can only be used directly WITHIN a react component.
// >>> Do not nest them in other function.
import ExpenseDate from './ExpenseDate'
import Card from '../UI/Card'

const ExpenseItem = props => {
  // console.log('Rendering ExpenseItem...')

  // const [title, setTitle] = useState(props.title)

  // const titleHandler = event => {
  //   console.log(title, 'before')
  //   setTitle(title + ' clicked!')
  //   console.log(title, 'after')
  //   console.log(event) // >>> notes: component functions will always have access to an event object
  //   // >>> notes:
  //   // >>> on FIRST click, you'll see "title before", "title after", "SyntheticBaseEvent {}"
  //   // >>> only on SECOND click you'll see "title clicked! before", "title clicked! after", "SyntheticBaseEvent {}"
  //   // >>> this is because when "titleHandler" is invoked (by clicking), React will run everything in titleHandler and when it hits setTitle(), React "schedules" a re-render for ExpenseItem function (component) to be run AFTER titleHandler is finished.
  // }


  return (
    <li>
      <Card className="expense-item">
        <ExpenseDate date={props.date}/>
        <div className="expense-item__description">
          <h2>{props.title}</h2>
          <div className="expense-item__price">${props.amount}</div>
        </div>
      </Card>
    </li>
  )
}

export default ExpenseItem

// >>> notes:
// >>> Notice on line 11, we declare a constant "title" with array destructuring, but the value of "title" kept being updated everytime titleHandler is invoked.
// >>> This is possible because we did not manually "reassign" the value of "title".
// >>> The constant "title" is being managed by React under the hood.

// >>> When ExpenseItem is being rendered/re-rendered, setTitle() will have a different SCOPE compared to the last time!
// >>> So altho the function name remains the same, React manages the ACTUAL STATE internally!
