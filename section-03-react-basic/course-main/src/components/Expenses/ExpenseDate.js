import './ExpenseDate.css'

const ExpenseDate = props => {
  const month = props.date.format('MMM')
  const year  = props.date.format('YYYY')
  const day   = props.date.format('DD')

  return (
    <div className="expense-date">
      <div className="expense-date__month">{month}</div>
      <div className="expense-date__year">{year}</div>
      <div className="expense-date__day">{day}</div>
    </div>
  )
}

export default ExpenseDate
