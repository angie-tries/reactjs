import ExpenseItem from './ExpenseItem'
import './ExpenseList.css'

const ExpenseList = props => {
  if (props.entries.length === 0) {
    return <h2 className="expenses-list__fallback">No expenses found.</h2>
  } else {
    return (
      <ul className="expenses-list">
        {props.entries.map(entry => (
          <ExpenseItem
            key={entry.id}
            title={entry.title}
            date={entry.date}
            amount={entry.amount}
          />
        ))}
      </ul>
    )
  }
}

export default ExpenseList
