import { useState } from 'react'
import './Expenses.css'
import ExpenseFilter from './ExpenseFilter'
import ExpenseList from './ExpenseList'
import ExpenseChart from './ExpenseChart'
import Card from '../UI/Card'

const Expenses = props => {
  const [year, setYear] = useState('2019')

  const updateYear = yearData => {
    setYear(prevData => {
      return yearData
    })
  }

  let filteredEntries = props.entries.filter(entry => entry.date.get('year').toString() === year)

  return (
    <div>
      <Card className="expenses">
        <ExpenseFilter
          filterByYear={updateYear}
          selectedYear={year}
        />
        <ExpenseChart entries={filteredEntries} />
        <ExpenseList entries={filteredEntries} />
      </Card>
    </div>
  )
}

export default Expenses
