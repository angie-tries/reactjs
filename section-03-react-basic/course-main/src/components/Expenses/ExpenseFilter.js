import React from 'react'

import './ExpenseFilter.css'

const ExpenseFilter = props => {
  const dropDownHandler = event => {
    props.filterByYear(event.target.value)
  }

  return (
    <div className='expenses-filter'>
      <div className='expenses-filter__control'>
        <label>Filter by year</label>
        <select
          onChange={dropDownHandler}
          value={props.selectedYear}>
          <option value='2022'>2022</option>
          <option value='2021'>2021</option>
          <option value='2020'>2020</option>
          <option value='2019'>2019</option>
        </select>
      </div>
    </div>
  )
}

export default ExpenseFilter


// >>> notes:
// >>> ExpenseFilter is a controlled component because its state is managed in the parent component.

// >>> dumb vs smart
// >>> stateless vs stateful
// >>> presentational vs functional
// >>> The former of the 3 sets above just means that the component has no state/is not managing state.
// >>> It's good to separate components into smaller chunks where you can manage whether its for UI or actual business logic.
