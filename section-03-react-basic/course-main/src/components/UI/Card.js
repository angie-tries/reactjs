// >>> notes:
// >>> ("cards" in this context is just a way of styling.)
// >>> Building components with this Card component (at this point), will just apply the same styling to its parent.

import './Card.css'

const Card = props => {
  const combinedClasses = 'card ' + props.className // className can be accessed thru the props object

  return (
    <div className={combinedClasses}>{props.children}</div>
    // will be rendered as <div class="card expense"> or <div clas="card expense-item">
  )
}

export default Card
