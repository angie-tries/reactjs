import dayjs from 'dayjs'
import Expenses from './components/Expenses/Expenses'
import NewExpense from './components/NewExpense/NewExpense'
import React, { useState } from 'react'

const sampleEntries = [
  {
    id: 'e1',
    title: 'React Course',
    date: dayjs('2020-12-30'),
    amount: '9.99'
  },
  {
    id: 'e2',
    title: 'Boba 4U',
    date: dayjs('2021-03-28'),
    amount: '17.99'
  },
  {
    id: 'e3',
    title: 'OEM caps',
    date: dayjs('2021-02-16'),
    amount: '10'
  },
  {
    id: 'e4',
    title: 'Cherry stabs',
    date: dayjs('2021-01-29'),
    amount: '12'
  }
]

const App = () => {
  const [expEntries, setExpEntries] = useState(sampleEntries)

  const addToExpenses = newExpenseObj => {
    setExpEntries(prevEntries => [newExpenseObj, ...prevEntries])
  }

  return (
    <div>
      <NewExpense insertNewData={addToExpenses} />
      <Expenses entries={expEntries} />
    </div>
  );


  // >>> notes: "alternative" (under the hood) way of creating JSX with .createElement()
  // >>> takes AT LEAST TWO ARGUMENTS:
  // >>> 1st argv: ELEMENT to create
  // >>> 2nd argv: the PROPS with kv pair, empty object if none
  // >>> 3rd argv and onwards (optional if none): CONTENT between the created element

  // >>> So to create the above JSX with the .createElement() method:
  // return React.createElement(
  //   'div',
  //   {},
  //   React.createElement('h2', {}, 'Let\'s get started!'),
  //   React.createElement(Expenses, {entries: expenses})
  // )
}

export default App;
