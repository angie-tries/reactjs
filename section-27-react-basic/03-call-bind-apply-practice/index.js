// >>> notes: call bind apply function refresher
// >>> All javascript functions comes with these 3 methods .call(), .bind(), and .apply()


// >>> setting up target object
const address = {
  street:  'Westermarkt 20',
  zipCode: '1016 GV',
  country: 'NL'
}

// >>> setting up window object
window.street  = 'window street'
window.zipCode = 'window zip'
window.country = 'window country'


// >>> setting up target function for this exercise
function getFullAddress (...argvs) {
  let result = this.street + ' ' + this.zipCode + ' ' + this.country + '.'

  if (argvs.length) {
    result += ` (ARGV: ${argvs.join(', ')})`
  }

  return result
}

// >>> notes: CONTEXT
// >>> This only work when function is set up using the "function" keyword, ie:
// >>> Assigning an annonymous function to a var using var, let, const; OR creating a named function.

// >>> In client-side JS, all functions that are not explicitly written within an object (not a method),
// >>> will be located in the "global environment", with "this" within the function pointing to the "window" object.

// >>> Global arrow functions WILL NOT WORK as expected in this context.
// >>> Because global arrow functions will ALWAYS point to the "window" object.

// >>> Setting up functions using the "function" keyword will always point "this" within its scope to the "window" object.
// >>> Even when the function is written WITHIN an object as a METHOD.
// >>> Hence, you would see "var self = this;" OUTSIDE of the method before "self" is used within the method body.

// >>> Arrow functions are created to solve "this" issue when used as a METHOD within an object.
// >>> When methods are written with arrow functions, "this" within the arrow function scopes will always point to the object.
// >>> So we do not have to "var self = this;" before referencing the object in our methods.


console.log('.call() results:')
console.log(getFullAddress())  // window street window zip window country.
console.log(getFullAddress.call(address))  // Westermarkt 20 1016 GV NL.
console.log(getFullAddress.call(address, 'multiple', 'arguments'))  // Westermarkt 20 1016 GV NL. (ARGV: multiple, arguments)
console.log('\n')
// >>> .call() INVOKES the targeted function with "this" within the function pointed to the targeted object (passed in as argument).
// >>> Accepts multiple parameters.
// >>> USECASE:  "borrow" functions/methods (WITHOUT ARGUMENTS) from another object to get results.


console.log('.bind() results:')
const copyOfGetAddress = getFullAddress.bind(address)
console.log(getFullAddress())  // window street window zip window country.
console.log(copyOfGetAddress())  // Westermarkt 20 1016 GV NL.
console.log(getFullAddress === copyOfGetAddress)  // false
console.log('\n')
// >>> .bind() RETURNS A COPY of the targeted function with "this" within the returned function pointed to the targeted object (passed in as argument).
// >>> USECASE: Duplicate functions/methods.


console.log('.apply() results:')
console.log(getFullAddress())  // window street window zip window country.
console.log(getFullAddress.apply(address))  // Westermarkt 20 1016 GV NL.
console.log(getFullAddress.apply(address, ['hello', 'world']))  // Westermarkt 20 1016 GV NL. (ARGV: hello, world)
console.log('\n')
// >>> .apply() INVOKES the targeted function with the ability to take a second parameter (array) as argument.
// >>> That array will hold all the arguments to be passed to the targeted function.
// >>> USECASE: "borrow" functions/methods (WITH ARGUMENTS) from another object to get results.
