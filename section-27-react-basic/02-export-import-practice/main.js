// >>> notes: default + named ex/import
// >>> The line <import React, { Component } from 'react'> means that package 'react'
// >>> gives you one DEFAULT export: React; and one NAMED export: Component.
// >>> And it seems like all named exports from one file are being wrapped into ONE OBJECT.


// // Example A:
// import main, { sayHello } from './a.js'
// main.innerFn() // main function called
// sayHello()     // hello

// // Example B:
import main, { sayHello, role } from './a.js'
console.log(role) // student
main.innerFn()    // main function called
sayHello()        // hello

// // Example C:
// import * as a from './a.js'
// console.log(a)
// // [Module: null prototype] {
// //   default: { innerFn: [Function: innerFn] },
// //   role: 'student',
// //   sayHello: [Function: sayHello]
// // }


// >>> view a.js file for more notes.
