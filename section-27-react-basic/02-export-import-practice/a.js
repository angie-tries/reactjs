// >>> notes:
// >>> To have BOTH types (named and default) of exports,
// >>> default export must be written after all your named exports.


// setting up object with one function for default export
const main = {
  innerFn () {  // arrow function works too
    console.log('main function called')
  }
}

// named export: function
export const sayHello = () => {
  console.log('hello')
}

// named export: string
export const role = 'student'

// default export on LAST LINE
export default main
