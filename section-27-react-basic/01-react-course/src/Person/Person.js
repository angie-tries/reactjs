// >>> notes: component core
// >>> At its core, a component is simply a function that returns some JSX

import React from 'react' // because we're creating our own component, so there's no need to import the { Component }
import './Person.css'

const person = props => {
  // >>> notes: props are basically attributes and values being passed into the component when used.
  return (
    <div className="Person">
      <p onClick={props.myHandlerFn}>I'm {props.name}, and i am {props.age} year old.</p>
      <p>{props.children}</p>
      <input type="text" onChange={props.changed} value={props.name}/>
      {/* >>> notes: value attribute must be paired with onChange attribute to avoid value locking */}
    </div>
  )
  // >>> notes: Class
  // >>> When the custom component is class-based (ie: class Person extends Component), remember to use {this.props.propsNameHere}, so the props are referrenced correctly.
  // >>> class-based components = class Person extends Component
  // >>> functional components = just a regular function that returns JSX
}

export default person

// >>> notes:
// >>> a component WITHOUT state management, can also be called "stateless" / "dumb" / "presentational" component.
