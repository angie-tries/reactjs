import React, { Component } from 'react';
import './App.css';

import Person from './Person/Person'
// >>> notes: importing our custom Person component
// >>> Component names should be capitalized so you can use it in the JSX like <Person />, and it wouldn't interfere with the rest of the codes.

// >>> notes: IMPORTANT
// >>> class based component setups are more traditional.
// >>> and if you must use functional components with state management, this is how you re-write:
class App extends Component {
  // >>> notes: state reserved keyword
  // >>> 'state' is a variable that is only available for class-based components (class Xyz extends Component).
  // >>> changes in props/state value will trigger DOM re-renders. (a little like watchers in Vue)
  state = {
    persons: [
      { name: 'angie', age: 28 },
      { name: 'max', age: 29 },
    ],
    otherState: 'some other value'
  }

  switchNameHandler = newName => {
    // this.state.persons[0] = 'OMG' // <<< DOESN'T WORK. Throws warning "Do not mutate state directly."
    this.setState({
      persons: [
        { name: 'angie', age: 28 },
        { name: newName, age: 100 },
      ]
    })
    // >>> notes: setState method
    // >>> This method accepts one object as a parameter, and in that object, user define the value needed to be updated.
  }

  nameChangeHandler = event => {
    this.setState({
      persons: [
        { name: 'angie', age: 28 },
        { name: event.target.value, age: 30 }
      ]
    })
    // console.log(event)
  }

  render () {
    const btnStyle = {
      padding: '10px 20px',
      borderRadius: '3px',
      border: '1px solid #333',
      cursor: 'pointer'
    }

    return (
      <div className="App">
        <h1>Hi, i am a react app</h1>
        <p>It really works!</p>
        <button
        onClick={() => this.switchNameHandler('max2')}
        style={btnStyle}>Switch Name</button> {/* >>> notes: event handlers here are unlike Vue, they are passed without parenthesis. */}
        {/* <Person /> // >>> notes: <p>I'm a person</p> this will just be whatever that's returned from the person() in Person.js */}
        <Person
          name={this.state.persons[0].name}
          age={this.state.persons[0].age}
          myHandlerFn={this.switchNameHandler.bind(this, 'max3')}>Hobby: Climbing</Person>
        <Person
          name={this.state.persons[1].name}
          age={this.state.persons[1].age}
          changed={this.nameChangeHandler}/>
        <p>{this.state.otherState}</p>{/* some other value */}
      </div>
    );
  }

    // >>> notes: react create element
    // >>> after 'null', everything else is being put into the element created by react ('div', in this case).
    // return React.createElement('div', null, 'h1', '<br>hi i am react!')  // <div>h1<br>hi i am react!</div>

    // >>> call React.createElement again in the 3rd argument to create and insert content into the element
    // return React.createElement('div', null, React.createElement('h1', null, 'hi i am still react!')) // <div><h1>hi! i am still react!</h1></div>

    // >>> to add class to the div, replace the 'null' with a javascript object
    // return React.createElement('div', { className: 'App' }, React.createElement('h1', { className: "hello" }, 'it should work now')) // <div class="App"><h1 class="hello">hi! i am still react!</h1></div>

    // >>> notes: JSX restrictions
    // >>> 1. certain keywords are restricted from being used in the return statement. Eg: 'class'
    // >>> Hence 'className' is used instead of 'class'.

    // >>> 2. only ONE element can be returned
    // >>> It is best practice to return only one element. Wrap them up!
}

export default App;

// >>> notes:
// >>> a component WITH statemanagement, can also be called "stateful" / "smart" / "container" component.
