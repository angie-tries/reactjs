// >>> setting up target object
const address = {
  country: 'NL',
  whoami1: function whoami () {
    console.log(this)
  },
  whoami2: () => {
    console.log(this)
  },

  nestedCountry1: function () {
    return function () {
      console.log(this.country)
    }
  },
  nestedCountry2: function () {
    return () => {
      console.log(this.country)
    }
  },

  nestedCountry3: () => {
    return () => {
      console.log(this.country)
    }
  },
  nestedCountry4: () => {
    return function () {
      console.log(this.country)
    }
  }
}

const newAddress = { country: 'DE' }
window.country = 'window'

function globalWhoami1 () {
  console.log(this)
}

const globalWhoami2 = () => {
  console.log(this)
}

address.whoami1()  // {country: NL}
address.whoami2()  // {window}
globalWhoami1()    // {window}
globalWhoami2()    // {window}
console.log('\n')

address.nestedCountry1()()  // window
address.nestedCountry2()()  // NL
address.nestedCountry3()()  // window
address.nestedCountry4()()  // window
console.log('\n')

const whoareyou1 = address.whoami1.bind(newAddress)
const whoareyou2 = address.whoami2.bind(newAddress)
whoareyou1() // {country: DE}
whoareyou2() // {window}

const locateThis1 = address.nestedCountry1().bind(newAddress)
const locateThis2 = address.nestedCountry2().bind(newAddress)
const locateThis3 = address.nestedCountry3().bind(newAddress)
const locateThis4 = address.nestedCountry4().bind(newAddress)

locateThis1() // DE
locateThis2() // NL
locateThis3() // window
locateThis4() // DE
console.log('\n')


class Location {
  constructor () {
    this.country = 'US'

    this.myNameIs1 = function () {
      console.log(this)
    }

    this.myNameIs2 = () => {
      console.log(this)
    }
  }
}

const netherlands = new Location()
netherlands.myNameIs1()  // {Location}
netherlands.myNameIs2()  // {Location}

netherlands.whoami = address.whoami1.bind(netherlands)
netherlands.whoami()  // {Location}

netherlands.whoami = address.whoami2.bind(netherlands)
netherlands.whoami() // {window}

netherlands.globalWhoami1 = globalWhoami1
netherlands.globalWhoami2 = globalWhoami2
netherlands.globalWhoami1() // {Location}
netherlands.globalWhoami2() // {window}
