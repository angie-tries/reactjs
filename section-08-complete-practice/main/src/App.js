import { useState } from 'react';
import UserInput from './components/Users/UserInput'
import UserList from './components/Users/UserList'

function App() {
  const [users, setUsers] = useState([])

  const addToUsers = data => {
    setUsers(prevArr => {
      const updatedArray = [...prevArr]
      updatedArray.unshift(data)
      return updatedArray
    })
  }

  return (
    <>
      <UserInput submitHandler={addToUsers} />
      <UserList entries={users} />
    </>
  );
}

export default App;
