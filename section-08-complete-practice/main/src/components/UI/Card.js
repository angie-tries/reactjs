import styles from './Card.module.sass'

const Card = props => {
  return (
    <div className={`${styles.card + ' ' + props.className}`}>
    {/* <div className={`${styles.card} ${props.className}`}> */}
      {props.children}
    </div>
  )
}

export default Card
