import styles from './Button.module.sass'

const Button = props => {
  return (
    <button
      className={styles.button}
      type={props.btnType || 'button'}
      onClick={props.onClick}>
      {props.children}
    </button>
  )
}

export default Button
