import ReactDOM from 'react-dom'
import styles from './ErrorModal.module.sass'
import Button from './Button'
import Card from './Card'

const Backdrop = props => {
  return (
    <div
      className={styles.backdrop}
      onClick={props.clearError}>
    </div>
  )
}

const Overlay = props => {
  return (
    <Card className={styles.modal}>
      <header className={styles.header}>
        <h2>{props.error.title}</h2>
      </header>
      <div className={styles.content}>
        <p>{props.error.message}</p>
      </div>
      <footer className={styles.actions}>
        <Button onClick={props.clearError}>Okay</Button>
      </footer>
    </Card>
  )
}

const ErrorModal = props => {
  return (
    <>
      {ReactDOM.createPortal(
        <Backdrop clearError={props.clearError} />,
        document.getElementById('backdrop-root')
      )}
      {ReactDOM.createPortal(
        <Overlay
          clearError={props.clearError}
          error={props.error}
        />,
        document.getElementById('overlay-root')
      )}
    </>
  )
}

export default ErrorModal
