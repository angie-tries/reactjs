import { useState, useRef } from 'react'
import styles from './UserInput.module.sass'
import Card from '../UI/Card'
import Button from '../UI/Button'
import ErrorModal from '../UI/ErrorModal'
import Wrapper from '../Helper/Wrapper'

const UserInput = props => {
  const nameInputRef = useRef()
  const ageInputRef = useRef()
  // >>> notes:
  // >>> useRef() only works in smart/functional component.
  // >>> It gives you access to other DOM elements and work with them.
  // >>> The function returns you the actual DOM, similar to document.getElById().

  const [validationError, setValidationError] = useState('')

  const errorInfo = {
    name: {
      title: 'Invalid name input',
      message: 'Name cannot be empty.'
    },
    age: {
      title: 'Invalid age input.',
      message: 'Age should be number > 0'
    },
    both: {
      title: 'Invalid name and age input.',
      message: 'Name cannot be empty, age should be number > 0'
    }
  }

  const addToUserArray = event => {
    event.preventDefault()
    const inputName = nameInputRef.current.value
    const inputAge = ageInputRef.current.value

    if (inputName.trim() === '' && Number(inputAge) <= 0) {
      setValidationError('both')
      return
    }

    if (inputName.trim() === '') {
      setValidationError('name')
      return
    }

    if (Number(inputAge) <= 0) {
      setValidationError('age')
      return
    }

    props.submitHandler({
      id: Math.random().toString(),
      name: inputName,
      age: inputAge
    })

    nameInputRef.current.value = ''
    ageInputRef.current.value = ''
    // >>> notes:
    // >>> While it is generally frowned upon to use "useRef()" to manipulate values,
    // >>> using it to clear values is still "forgivable".

    // >>> Again, because useRef() returns you the actual DOM, so mutating the value from refs are not the best practice although it's much lesser code.
    // >>> You would want to use "useState" to manage the value instead.

    // >>> With all these being said, if you only need to READ a value, but never planned on mutating the value, then useRef() will be a better use case.
  }

  const clearError = () => {
    setValidationError('')
  }

  return (
    <Wrapper>
      {validationError && (
        <ErrorModal
          error={errorInfo[validationError]}
          clearError={clearError} />
      )}
      {/* notes: again, && returns first FALSY value */}
      <Card className={styles.input}>
        <form
          onSubmit={addToUserArray}
          className="submission">
          <label htmlFor="nameInput">Username</label>
          <input
            id="nameInput"
            type="text"
            ref={nameInputRef} />
          <label htmlFor="ageInput">Age (Year)</label>
          <input
            id="ageInput"
            type="number"
            ref={ageInputRef} />
          <Button btnType="submit">Add User</Button>
        </form>
      </Card>
    </Wrapper>
  )
}

export default UserInput
