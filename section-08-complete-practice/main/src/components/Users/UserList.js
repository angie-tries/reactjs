import Card from '../UI/Card'
import styles from './UserList.module.sass'
import UserListItem from './UserListItem'

const UserList = props => {
  return (
    <Card className={styles.users}>
      <ul>
        {props.entries.map(entry => (
          <UserListItem
            key={entry.id}
            name={entry.name}
            age={entry.age}
          />
        ))}
      </ul>
    </Card>
  )
}

export default UserList
