import styles from './UserList.module.sass'

const UserListItem = props => {
  return (
    <li className={styles.users}>
      {props.name} ({props.age} years old)
    </li>
  )
}

export default UserListItem
