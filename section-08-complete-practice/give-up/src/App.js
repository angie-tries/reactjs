import { useState } from 'react';
import UserInput from './components/entry/UserInput'
import UserList from './components/entry/UserList'

function App () {
  const [users, setUsers] = useState([])

  const addUser = data => {
    console.log(data)
    setUsers(prevArray => {
      const updatedUsers = [...prevArray]
      updatedUsers.unshift(data)
      return updatedUsers
    })
  }

  return (
    <div>
      <UserInput submitUserInput={addUser} />
      <UserList entries={users} />
    </div>
  );
}

export default App;
