const ErrorModal = props => {
  return (
    <div>
      <p>{props.errorInfo.error === true ? props.errorInfo.message : ''}</p>
    </div>
  )
}

export default ErrorModal
