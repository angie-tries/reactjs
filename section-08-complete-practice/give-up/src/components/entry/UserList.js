const UserList = props => {
  return (
    <ul>
      {props.entries.length === 0 ? <p>create entries!</p> : ''}
      {props.entries.map(entry => (
        <li
          key={entry.id}
        >{entry.name} {entry.age}
        </li>
      ))}
    </ul>
  )
}

export default UserList
