import { useState } from 'react'

const UserInput = props => {
  const [userName, setUsername] = useState('')
  const [userAge, setUserAge] = useState('')
  const [invalidUsername, setInvalidUsername] = useState(true)
  const [invalidAge, setInvalidAge] = useState(true)
  const [invalidUsernameMsg, setInvalidUsernameMsg] = useState('')
  const [invalidAgeMsg, setInvalidAgeMsg] = useState('')

  const usernameInputHandler = event => {
    setUsername(event.target.value.trim())
  }

  const ageInputHandler = event => {
    setUserAge(event.target.value)
  }

  const usernameValidator = () => {
    if (userName.trim() === '') {
      setInvalidUsername(true)
      setInvalidUsernameMsg('Username cannot be blank.')
      return
    }

    setInvalidUsername(false)
    setInvalidUsernameMsg('')
  }

  const ageValidator = () => {
    if (Number(userAge) < 0) {
      setInvalidAge(true)
      setInvalidAgeMsg('Age must be larger than 0.')
      return
    }

    if (userAge.trim() === '') {
      setInvalidAge(true)
      setInvalidAgeMsg('Age cannot be blank.')
      return
    }

    setInvalidAge(false)
    setInvalidAgeMsg('')
  }

  const submitHandler = event => {
    event.preventDefault()
    console.log(invalidUsername, invalidAge)

    if (invalidUsername || invalidAge) {
      return
    }

    props.submitUserInput({
      id: Math.random().toString(),
      name: userName,
      age: userAge
    })

    setUsername('')
    setUserAge('')
  }

  return (
    <form onSubmit={submitHandler}>
      <label>Username</label>
      <input
        type="text"
        value={userName}
        onChange={usernameInputHandler}
        onBlur={usernameValidator} />
      <p>{invalidUsernameMsg}</p>
      <br/>
      <label>Age (Years)</label>
      <input
        type="number"
        step="1"
        value={userAge}
        onChange={ageInputHandler}
        onBlur={ageValidator} />
      <p>{invalidAgeMsg}</p>
      <button type="submit">Add User</button>
    </form>
  )
}

export default UserInput
