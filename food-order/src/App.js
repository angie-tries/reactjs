import { useState } from 'react'
import Header from './components/Layout/Header'
import Meals from './components/Meals/Meals'
import Cart from './components/Cart/Cart'
import CartProvider from './store/CartProvider'

function App() {
  const [displayCart, setDisplayCart] = useState(false)

  const cartToggle = () => {
    setDisplayCart(cartState => !cartState)
  }

  return (
    <CartProvider>
      {displayCart && <Cart onCartToggle={cartToggle}/>}
      <Header onCartToggle={cartToggle} />
      <main>
        <Meals />
      </main>
    </CartProvider>
  )
}

export default App
