import { useReducer } from 'react'
import CartContext from './cart-context'

const defaultCartState = {
  items: [],
  grandTotal: 0
}

const cartReducer = (state, action) => {
  if (action.type === 'ADD_TO_CART') {
    const itemIndexInCart = state.items.findIndex(item => item.id === action.item.id)
    const updatedTotal = state.grandTotal += action.item.price * action.item.qty
    let itemInCart
    let updatedItems

    if (itemIndexInCart !== -1) {
      itemInCart = {
        ...state.items[itemIndexInCart],
        qty: state.items[itemIndexInCart].qty + action.item.qty
      }

       updatedItems = [...state.items]
       updatedItems[itemIndexInCart] = itemInCart
    } else {
      updatedItems = state.items.concat(action.item)
    }

    return {
      items: updatedItems,
      grandTotal: updatedTotal
    }
  }

  if (action.type === 'REMOVE_FR_CART') {
    const itemIndexInCart = state.items.findIndex(item => item.id === action.id)
    let updatedItems
    let updatedTotal

    if (itemIndexInCart !== -1) {
      const itemInCart = state.items[itemIndexInCart]
      updatedTotal = state.grandTotal - itemInCart.price

      if (itemInCart.qty === 1) {
        // remove entire item if removing last of kind
        updatedItems = state.items.filter(item => item.id !== action.id)
      } else {
        const decrementedItem = {
          ...itemInCart,
          qty: itemInCart.qty - 1
        }
        updatedItems = [...state.items]
        updatedItems[itemIndexInCart] = decrementedItem
      }
    }

    return {
      items: updatedItems,
      grandTotal: updatedTotal
    }
  }

  return defaultCartState
}

const CartProvider = props => {
  const [cartState, dispatchCartAction] = useReducer(cartReducer, defaultCartState)

  const addToCart = item => {
    dispatchCartAction({
      type: 'ADD_TO_CART',
      item,
    })
  }

  const removeFrCart = id => {
    dispatchCartAction({
      type: 'REMOVE_FR_CART',
      id,
    })
  }

  const contextObj = {
    items: cartState.items,
    grandTotal: cartState.grandTotal,
    addItem: addToCart,
    removeItem: removeFrCart
  }

  return (
    <CartContext.Provider value={contextObj}>
      {props.children}
    </CartContext.Provider>
  )
}

export default CartProvider
