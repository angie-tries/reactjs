import ReactDOM from 'react-dom'
import classes from './Modal.module.css'

const Backdrop = props => {
  return (
    <div className={classes.backdrop} onClick={props.onClick}/>
  )
}

const ModalOverlay = props => {
  return (
    <div className={classes.modal}>
      <div className={classes.content}>{props.children}</div>
    </div>
  )
}

const portToEl = document.getElementById('overlays')

const Modal = props => {
  return (
    <>
      {/* <Backdrop />
      <ModalOverlay>{props.children}</ModalOverlay> */}
      {ReactDOM.createPortal(<Backdrop onClick={props.onClick}/>, portToEl)}
      {ReactDOM.createPortal(<ModalOverlay>{props.children}</ModalOverlay>, portToEl)}
    </>
  )
}

export default Modal
