import { useContext } from 'react'
import Modal from '../UI/Modal'
import classes from './Cart.module.css'
import CartContext from '../../store/cart-context'
import CartItem from './CartItem'

const Cart = props => {
  const contextObj = useContext(CartContext)
  const totalAmount = `$${contextObj.grandTotal.toFixed(2)}`

  const incrementHandler = item => {
    contextObj.addItem({
      ...item, qty: 1
    })
  }
  const decrementHandler = id => {
    contextObj.removeItem(id)
  }

  const cartItem = (
    <ul className={classes['cart-items']}>
      {
        contextObj.items.map(item => (
          <CartItem
            key={item.id}
            title={item.title}
            price={item.price}
            qty={item.qty}
            onIncrement={incrementHandler.bind(null, item)}
            onDecrement={decrementHandler.bind(null, item.id)}
            // using .bind() so the function receives necessary arguments but DO NOT execute it
          />
        ))
      }
    </ul>
  )

  return (
    <Modal onClick={props.onCartToggle}>
      {cartItem}
      <div className={classes.total}>
        <span>Total Amount:</span>
        <span>{totalAmount}</span>
      </div>
      <div className={classes.actions}>
        <button
          className={classes['button--alt']}
          onClick={props.onCartToggle}
        >
          Close
        </button>
        {contextObj.items.length > 0 && <button className={classes.button}>Order</button>}
      </div>
    </Modal>
  )
}

export default Cart
