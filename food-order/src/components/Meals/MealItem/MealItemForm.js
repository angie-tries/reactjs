import { useRef, useState } from 'react'
import classes from './MealItemForm.module.css'
import Input from '../../UI/Input'

const MealItemForm = props => {
  const inputQty = useRef()
  const [formValidity, setFormValidity] = useState(true)

  const submitHandler = event => {
    event.preventDefault()
    const enteredQty = Number(inputQty.current.value)

    if (enteredQty < 1 || enteredQty > 5) {
      setFormValidity(false)
      return
    }

    props.onAddToCart(enteredQty)
  }

  return (
    <form
      className={classes.form}
      onSubmit={submitHandler}>
      <Input
        ref={inputQty}
        label="Quantity"
        input={{
          id: 'qty_' + props.id,
          type: 'number',
          min: '1',
          max: '5',
          step: '1',
          defaultValue: '1'
        }}
      />
      <button className={classes.button}>+ Add</button>
      {formValidity === false && <p>Please choose number between 1 and 5 only.</p>}
    </form>
  )
}

export default MealItemForm
