import { useContext } from 'react'
import classes from './MealItem.module.css'
import MealItemForm from './MealItemForm'
import CartContext from '../../../store/cart-context'

const MealItem = props => {
  const contextObj = useContext(CartContext)
  const price = `$${props.price.toFixed(2)}`

  const addToCartHandler = quantity => {
    contextObj.addItem({
      id: props.id,
      title: props.title,
      price: props.price,
      qty: quantity
    })
  }

  return (
    <li className={classes.meal}>
      <div>
        <h3>{props.title}</h3>
        <div className={classes.description}>{props.desc}</div>
        <div className={classes.price}>{price}</div>
      </div>
      <div>
        <MealItemForm onAddToCart={addToCartHandler}/>
      </div>
    </li>
  )
}

export default MealItem
