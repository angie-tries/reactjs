import { useContext, useEffect, useState } from 'react'
import CartContext from '../../store/cart-context'
import CartIcon from '../Cart/CartIcon'
import classes from './HeaderCartButton.module.css'

const HeaderCartButton = props => {
  const contextObj = useContext(CartContext)
  const [btnAnimation, setBtnAnimation] = useState(false)

  const allItems = contextObj.items.reduce((accumulated, arrItem) => accumulated + arrItem.qty, 0)

  const buttonClasses = `${classes.button} ${btnAnimation ? classes.bump : ''}`

  useEffect(() => {
    if (contextObj.items.length === 0) {
      // so button doesn't bounce when there's nothing in cart
      return
    }

    setBtnAnimation(true)

    const removalTimer = setTimeout(() => {
      setBtnAnimation(false)
    }, 300) // 300 because the animation duration is 300

    return () => {
      clearTimeout(removalTimer)
    }
  }, [contextObj.items])


  return (
    <button
      onClick={props.onClick}
      className={buttonClasses}
    >
      <span className={classes.icon}>
        <CartIcon />
      </span>
      <span>Your Cart</span>
      <span className={classes.badge}>
        {allItems}
      </span>
    </button>
  )
}

export default HeaderCartButton
