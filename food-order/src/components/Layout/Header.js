import HeaderCartButton from './HeaderCartButton'
import mealsJpg from '../../assets/meals.jpg'
import classes from './Header.module.css'

const Header = props => {
  return <>
    <header
      className={classes.header}
    >
      <h1>ReactNoms</h1>
      <HeaderCartButton onClick={props.onCartToggle}/>
    </header>
    <div
      className={classes['main-image']}
    >
      <img
        src={mealsJpg}
        alt="Buffet"
      />
    </div>
  </>
}

export default Header
