class Human {
  constructor () {
    this.gender = 'female'
  }
}

class Person extends Human {
  constructor () {
    super() // this function needs to be called, to run the constructor() from its parent (Human) when a new Person is initated
    this.name   = 'alex'
    this.gender = 'non binary'
  }

  // methods are functions written without 'function' keywords
  // function say (msg) {
  //   console.log(msg)
  // }

  printName () {
    console.log(this.name)
  }
}

const person = new Person()
person.printName()          // alex
console.log(person.gender)  // non binary
