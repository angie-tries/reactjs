// default export
const person = {
  name: 'alex'
}

export default person

// 'default' keyword allows use to import this file and assign person object to any variable name
