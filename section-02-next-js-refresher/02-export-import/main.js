import person from './person.js'
import orang from './person.js'

import { clean, data, data as number } from './util.js'
import * as everything from './util.js'

console.log(person)      // { name: 'alex' }
console.log(orang)       // { name: 'alex' }
console.log(clean)       // [Function: clean]
console.log(data)        // 10
console.log(number)      // 10
console.log(everything)  // [Module: null prototype] { clean: [Function: clean], data: 10 }

// open the other files to read notes
