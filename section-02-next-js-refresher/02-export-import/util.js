// named export
export const clean = () => {}

export const data = 10

// these are called 'named exports'.
// meaning that there are two 'things' being exported in this file.
// and users have to specify which one to use by their names
