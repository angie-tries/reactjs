// ===== Refresher practice
// ===== Example one: the "this" here all referring to window object when bare function is declared and called

// function myfn () {
//   console.log(this)
// }

// myfn()

// const myfn2 = () => {
//   console.log(this)
// }

// myfn2()



// ===== Example two: "this" makes a huge different when called in objects
function Person (name, age) {
  this.name = name
  this.age = age
  this.say = function say (msg) {
    console.log(this, '----')
    console.log(msg + ', i\'m ' + this.name)
  }

  function say (msg) {
    this.name = msg
    console.log(this) // 'this' here always refers back to the windows object because named function
    console.log(msg + ', my name is ' + this.name)
  }
  console.log(say('oh'))

  const say2 = msg => {
    // this.name = msg // overrides the value passed in when initializing Person object
    this.name2 = msg
    console.log(this, 'arrow')
    console.log(msg + ', my name iz ' + this.name)
  }
  console.log(say2('ehlos'))
}

const john = new Person('john', 10)
const jane = new Person('jane', 18)

console.log(john.say('hello'))
console.log(jane.say('hello'))
