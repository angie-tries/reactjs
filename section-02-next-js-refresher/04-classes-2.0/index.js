class Human {
  gender = 'female'
}

class Person extends Human {
  name   = 'alex'
  gender = 'non binary'

  printName = () => {
    console.log(this.name)
  }
}

const person = new Person()
person.printName()          // alex
console.log(person.gender)  // non binary
