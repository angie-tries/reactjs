// ...

// spread operators can be used to remap arrays and objects

const arr1 = [1, 2, 3]
const arr2 = ['c', 'd', 'e']
const arr3 = [...arr1, ...arr2]
console.log(arr3) // [ 1, 2, 3, 'c', 'd', 'e' ]

const obj1 = { a: 1, b: 2, c: 3 }
const obj2 = { c: 4, d: 5, e: 6 }
const obj3 = {...obj1, ...obj2}
const obj4 = {...obj2, ...obj1}
console.log(obj3) // { a: 1, b: 2, c: 4, d: 5, e: 6 }
console.log(obj4) // { c: 3, d: 5, e: 6, a: 1, b: 2 }
// notice the key 'c' has different values in both obj3 and obj4.
// the value of 'c' in obj4 depends on which value comes in LAST to override.

const obj5 = {...obj1, c: 'hello'}
console.log(obj5) // {  a: 1, b: 2, c: 'hello' }


// rest operator

const myFilter = (...argvs) => {
  // ... wraps all arguments into an array
  // so there's no need to for loop them prior to use
  return argvs.filter(el => el === 1)
}

console.log(myFilter(1, 2, 3)) // [ 1 ]

// both spread and rest take out ALL elements and properties/elements and distributes them into a new object/array
