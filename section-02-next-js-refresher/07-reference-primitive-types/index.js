// passed by VALUE
let numA = 1
const numB = numA
console.log(numB, numA) // 1 1
console.log(numA === numB) // true
numA = 3
console.log(numA, numB) // 3 1
// when primitive types are used when declaring a variable, the value is copied into the new variable.
// Primitives in javascipts are passed by VALUE


// passed by REFERENCE
const personA = {
  name: 'alex'
}

const personB = personA
personB.name = 'angie'
console.log(personA.name, personB.name) // angie angie
console.log(personA === personB) // true
// personB is still pointing to personA
// so when value of personB is changed, you're essentially updating personA's value also, because they are referencing the same object underlyingly


const personC = {
  ...personA
}

personC.name = 'albie'
console.log(personC.name, personA.name) // albie angie
// spread operator is used to "make a copy" of an object
