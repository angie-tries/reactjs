// destructuring arrays
const num = [1, 2, 3]
const [num1, , num3] = num
console.log(num1, num3) // 1 3
// just declare variables within an array according to their positions in the targeted array

// destructuring objects
const obj = {
  person: 'alex',
  year: 1992
}

const { person, year } = obj
console.log(person, year) // alex, 1992
// the variable names should match the object keys
